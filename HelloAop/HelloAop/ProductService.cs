﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using HelloAop.Aspects;


namespace HelloAop
{
    public class ProductService
    {
        [LogMethod]        
        public void SaveProduct(string productName)
        {
            Console.WriteLine("Product saved in database!");
        }
    }
}
