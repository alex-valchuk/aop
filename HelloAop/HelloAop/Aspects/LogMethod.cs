﻿using System;

using PostSharp.Aspects;

namespace HelloAop.Aspects
{
    [Serializable]
    public class LogMethod : OnMethodBoundaryAspect
    {
        public override void OnEntry(MethodExecutionArgs args)
        {
            Console.WriteLine("LOG: '{0}' method started", args.Method.Name);
        }

        public override void OnExit(MethodExecutionArgs args)
        {
            Console.WriteLine("LOG: '{0}' method finished", args.Method.Name);
        }
    }
}
