﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloAop
{
    class Program
    {
        static void Main(string[] args)
        {
            var service = new ProductService();
            service.SaveProduct(null);

            Console.WriteLine("Press eny key...");
            Console.ReadLine();
        }
    }
}
