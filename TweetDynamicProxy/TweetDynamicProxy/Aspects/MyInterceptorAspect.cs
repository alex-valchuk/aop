﻿using System;

using Castle.DynamicProxy;

namespace TweetDynamicProxy.Aspects
{
    public class MyInterceptorAspect : IInterceptor
    {
        public void Intercept(IInvocation invocation)
        {
            Console.WriteLine("Started");
            invocation.Proceed();
            Console.WriteLine("Complete");
        }
    }
}
