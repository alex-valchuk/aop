﻿using System;

using Castle.DynamicProxy;

using TweetDynamicProxy.Aspects;

namespace TweetDynamicProxy
{
    class Program
    {
        static void Main(string[] args)
        {
            var proxyGenerator = new ProxyGenerator();
            var aspect = new MyInterceptorAspect();

            var svc = proxyGenerator.CreateClassProxy<TwitterClient>(aspect);
            svc.Send("hi");

            Console.ReadLine();
        }
    }
}
