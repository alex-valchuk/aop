﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransactionsCastle
{
    class Program
    {
        static void Main(string[] args)
        {
            var invoiceService = new InvoiceService();

            var invoice = new Invoice
            {
                InvoiceId = Guid.NewGuid(),
                InvoiceDate = DateTime.Now,
                Items = new List<string> { "Item1", "Item2", "Item3" }
            };

            invoiceService.Save(invoice);

            Console.WriteLine("Save successful");
            Console.ReadLine();
        }
    }
}
