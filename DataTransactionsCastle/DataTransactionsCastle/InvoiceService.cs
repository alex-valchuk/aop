﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransactionsCastle
{
    public class InvoiceService
    {
        public virtual void Save(Invoice invoice)
        {
        }

        bool _isRetry;
        public virtual void SaveRetry(Invoice invoice)
        {
            if (!_isRetry)
            {
                _isRetry = true;
                throw new DataException();
            }
        }

        public virtual void SaveFail(Invoice invoice)
        {
            throw new DataException();
        }
    }
}
