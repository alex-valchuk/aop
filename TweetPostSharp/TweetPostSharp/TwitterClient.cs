﻿using System;

using TweetPostSharp.Aspects;

namespace TweetPostSharp
{
    public class TwitterClient
    {
        [MyInterceptorAspect]
        public void Send(string msg)
        {
            Console.WriteLine("Sending: {0}", msg);
        }
    }
}
