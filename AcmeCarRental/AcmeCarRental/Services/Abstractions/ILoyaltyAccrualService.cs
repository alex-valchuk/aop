﻿using AcmeCarRental.BLL.Entities;

namespace AcmeCarRental
{
    public interface ILoyaltyAccrualService
    {
        void Accrue(RentalAgreement agreement);
    }
}
