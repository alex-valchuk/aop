﻿using AcmeCarRental.BLL.Entities;

namespace AcmeCarRental.BLL.Services.Abstractions
{
    public interface ILoyaltyRedemptionService
    {
        void Redeem(Invoice invoice, int numberOfDays);
    }
}
