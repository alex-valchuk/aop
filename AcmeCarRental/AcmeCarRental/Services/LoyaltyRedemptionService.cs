﻿using AcmeCarRental.BLL.Aspects;
using AcmeCarRental.BLL.Entities;
using AcmeCarRental.BLL.Data.Abstractions;
using AcmeCarRental.BLL.Services.Abstractions;

namespace AcmeCarRental.BLL.Services
{
    public class LoyaltyRedemptionService : ILoyaltyRedemptionService
    {
        private readonly ILoyaltyDataService loyaltyDataService;

        public LoyaltyRedemptionService(ILoyaltyDataService loyaltyDataService)
        {
            this.loyaltyDataService = loyaltyDataService;
        }

        [Logging]
        [Defensive]
        [Transaction]
        [ExceptionHandler]
        public void Redeem(Invoice invoice, int numberOfDays)
        {
            var pointsPerDay = 10;

            if (invoice.Vehicle.Size >= Size.Luxury)
            {
                pointsPerDay = 15;
            }

            var points = numberOfDays * pointsPerDay;

            this.loyaltyDataService.SubtractPoints(invoice.Customer.Id, points);
            invoice.Discount = numberOfDays * invoice.CostPerDay;
        }
    }
}
