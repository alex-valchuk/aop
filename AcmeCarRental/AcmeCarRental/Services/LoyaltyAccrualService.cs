﻿using System;

using AcmeCarRental.BLL.Aspects;
using AcmeCarRental.BLL.Entities;
using AcmeCarRental.BLL.Data.Abstractions;

namespace AcmeCarRental.BLL.Services
{
    public class LoyaltyAccrualService : ILoyaltyAccrualService
    {
        private readonly ILoyaltyDataService loyaltyDataService;

        public LoyaltyAccrualService(ILoyaltyDataService loyaltyDataService)
        {
            this.loyaltyDataService = loyaltyDataService;
        }

        [Logging]
        [Defensive]
        [Transaction]
        [ExceptionHandler]
        public void Accrue(RentalAgreement agreement)
        {
            var rentalTimeSpan = agreement.EndDate.Subtract(agreement.StartDate);

            var numberOfDays = (int)Math.Floor(rentalTimeSpan.TotalDays);
            var pointsPerDay = 1;

            if (agreement.Vehicle.Size >= Size.Luxury)
                pointsPerDay = 2;

            var points = numberOfDays * pointsPerDay;
            this.loyaltyDataService.AddPoints(agreement.Customer.Id, points);
        }
    }
}
