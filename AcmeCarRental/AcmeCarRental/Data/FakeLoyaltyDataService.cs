﻿using System;

using AcmeCarRental.BLL.Data.Abstractions;

namespace AcmeCarRental.BLL.Data
{
    public class FakeLoyaltyDataService : ILoyaltyDataService
    {
        public void AddPoints(Guid customerID, int points)
        {
            Console.WriteLine("Adding {0} points for customer '{1}'", points, customerID);
        }

        public void SubtractPoints(Guid customerID, int points)
        {
            Console.WriteLine("Subtracting {0} points for customer '{1}'", points, customerID);
        }
    }
}
