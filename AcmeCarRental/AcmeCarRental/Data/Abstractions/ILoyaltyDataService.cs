﻿using System;

namespace AcmeCarRental.BLL.Data.Abstractions
{
    public interface ILoyaltyDataService
    {
        void AddPoints(Guid customerID, int points);
        void SubtractPoints(Guid customerID, int points);
    }
}
