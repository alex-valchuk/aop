﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcmeCarRental.BLL.Entities
{
    public enum Size
    {
        Compact = 0,
        Midsize,
        FullSize,
        Luxury,
        Truck,
        SUV
    }
}
