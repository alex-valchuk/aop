﻿using System;

using PostSharp.Aspects;

using AcmeCarRental.BLL.Entities;

namespace AcmeCarRental.BLL.Aspects
{
    [Serializable]
    public class Logging : OnMethodBoundaryAspect
    {
        public override void OnEntry(MethodExecutionArgs args)
        {
            Console.WriteLine("{0} started: {1}", args.Method.Name, DateTime.Now);

            foreach (var argument in args.Arguments)
            {
                if (argument is RentalAgreement)
                {
                    var agreement = (RentalAgreement)argument;

                    Console.WriteLine("Customer: {0}", agreement.Customer.Id);
                    Console.WriteLine("Vehicle: {0}", agreement.Vehicle.Id);
                }

                if (argument is Invoice)
                    Console.WriteLine("Invoice: {0}", ((Invoice)argument).Id);
            }
        }

        public override void OnSuccess(MethodExecutionArgs args)
        {
            Console.WriteLine("{0} complete: {1}", args.Method.Name, DateTime.Now);
        }
    }
}
