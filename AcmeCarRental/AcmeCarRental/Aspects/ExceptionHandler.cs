﻿using System;

using PostSharp.Aspects;

namespace AcmeCarRental.BLL.Aspects
{
    [Serializable]
    public class ExceptionHandler : OnExceptionAspect
    {
        public override void OnException(MethodExecutionArgs args)
        {
            //if (Exceptions.Handle(args.Exception))
                args.FlowBehavior = FlowBehavior.Continue;
        }
    }
}
